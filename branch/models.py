from django.db import models
from django.contrib.Auth import User
from kb.models import *
from messaging.models import IM



# Create your models here.



class Branch(models.Model):
	name = models.CharField(max_length=30)
	address = models.TextField()
	phone = models.IntegerField(max_length=30)
	email = models.EmailField(max_length=30)
	admin = models.ForeignKey(BranchUser) # Must check weather is_branch_admin
	total_staffs = models.IntegerField(max_length = 10) # we have to calculate all users of a branch




		
class Designation(models.Model):
	"""docstring for designation"""
	name = models.CharField(max_length=30)
	stars = models.CharField(max_length = 30)
	icon = models.ImageField(max_length =30)
	
	

class Department(models.Model):
	"""docstring for Department"""
	name = models.CharField(max_length=30)
	desc = models.TextField()
	no_of_users = models.IntegerField() # count total users in various departments
     
		

class BranchUser(User):
	""" BranchAdmin Who have full Permission Inherting and Expanding 
	from Auth User """
    name = models.CharField(max_length=30)
    branch =models.ForeignKey(Branch)
    date_of_joining =models.DateField()
    designation = models.ForeignKey(Designation)
    department =models.ForeignKey(Department) # if 
    roles = models.OneToOneField(UserRoles) # User Groups added from admin
    avatar = models.ImageField(max_length = 100) # Webcam or Image upload
    gallery = models.TextField() # Multiple images
    linkedin = models.URLField()
    mobile = models.IntegerField(max_length = 20)
    email = models.EmailField(max_length=40)
    salary = models.IntegerField(max_length=50) 
    resume = models.FileField() # should be attached but not be visible for all only admins
    im = models.ForeignKey(IM) # internal communication messaging i think it can be added from this models outside
    

   
    







	



	