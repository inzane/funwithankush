from django.db import models

# Create your models here.
from branch.models import BranchUser


""" Check user group is_techie ( no need to show up entire project ) , is_techie_admin ( can create & assign ) """

class Project(models.Model):
	"""A project will have many Unique modules"""
	
	name = models.CharField(max_length=20)
	total_deadline = models.DateTimeField() # must be calculated from all modules
	desc = models.TextField()
	modules = models.ForeignKey(Modules) # one to many 


class Modules(models.Model):  # 
	"""One module will have only One project"""
	name = models.CharField(max_length=30)
	user = models.ManyToManyField(BranchUser) # Assigning Techies
	deadline = models.DateTimeField()
	comment = models.TextField() # Activity behid

